# TrackerDPGExercise

Tracker short exercise 

# Setup CMSSW area

cmsrel CMSSW_10_2_9

cd CMSSW_10_2_9/src/

cmsenv

git cms-init

git clone https://gitlab.cern.ch/sroychow/trackerdpgexercise.git TrackerDPG

scram b 
